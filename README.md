# eGov-health

Demonstration of a government-backed electronic Health Insurance service backoffice integrating with a GLASS Wallet.

# eHealth Backoffice Demo

This project consists of a typical REST services gateway, designed to interface with
the GLASS Wallet, plus an additional "classical" administrative web-app backoffice on top of a "classsical" relational SQL database.

The SQL database represents the "master" of data for a centralized identity database, on which, every citizen of a given country would have an entry, describing its civilian identity. The data model is actually based on ...

This database is confidential to the country's organization.
The data included in this demo's setup is to be considered "specimen data", and is provided for demonstration purpose.

## Architecture

TODO: all of it

This demo application is composed by a few major components, in separate folders.

To see how to setup a developer's working directory to contribute to each component, please read the README.md of each folder.

To quickly run an evaluation instance, please see the docker section at the end.

### backoffice-sql

The backoffice SQL database setup files, with initial "specimen data".

### backoffice-dsu

OpenDSU services and a legacy web-application REST services gateway.

### docker

The Dockerfile-ci is to be used by the gitlab-ci.yml to build the image for the runtime environments.
The docker folder contains files used during the container image build.






