# Backoffice DSU

Highlight of this folder sub-folders:

- apihub-root - an apihub server (that provides OpenDSU services) that run within a government agency that hosts the
  egovhealth backoffice.
  Although these services run within a government agency, the apihub-root services must be public, so that citizens may
  download dapps and dapps may use DSU services from the government agency.
  The file layout is based on https://gitlab.com/glass-project1/wallet/dsu-blueprint-workspace
  ... but having an extra egovhealth-dapp (instead of a demo-dapp).
- api - REST services needed for a legacy eGovId management application. (See backend-dsu/api/README.md for details).

## backoffice-dsu/api

This is a REST services server (written in NestJS) that integrates with OpenDSU, and is placed inside the backoffice-dsu
so that it can use (require) the OpenDSU modules.

## Developer instructions to setup a working directory.

This is an instance of https://gitlab.com/glass-project1/wallet/dsu-blueprint-workspace
adapted for egovhealth and has the same dependencies and setup instructions.

## Setting up the eGovId Glass Wallet source code

### src/index.html

This src/index.html is ... based on
from https://gitlab.com/glass-project1/wallet/demo-dapp/-/blob/master/code/index.html

### Install

```sh
npm install
```

### Start REST API

```sh
cd api
# npm installed was already performed by parent octous.json
npm run start
```

The output will log If during the REST activity on the terminal.

This will start the REST API. You can test it using the Swagger UI at http://localhost:3004/borest/api

If you see an error like
```txt
[Nest] 93493  - 08/24/2022, 2:59:56 PM   ERROR [ExceptionHandler] connect ECONNREFUSED 127.0.0.1:5432
Error: connect ECONNREFUSED 127.0.0.1:5432
```
it means that you need to run a local database. See the instructions at the top-level /backoffice-sql/README.md

This app will not be testable standalone. Needs egov running on localhost:3000/borest/api, and an apihub runing on localhost:8080
(You can get them from the egov project, and the apihub from the glass-wallet-workspace. Follow their README.md and start them.)
