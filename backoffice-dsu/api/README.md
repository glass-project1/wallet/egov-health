# backoffice-dsu/api

This includes only the REST services to access the SQL database.

A stack of nest-js, typeorm

The frontend web interface app is at 


## Developer instructions to setup a working directory.

### backoffice-dsu

If you have not started installing backoffice-dsu (parent directory), please start there. This installation will be parte of it.


### ../../backoffice-sql

First you need to setup a working copy of the SQL database.

Please read ../../backoffice-sql/README.md and only proceed when you have PostgreSQL running.

### backoffice-dsu/backoffice-backend

```sh
npm install
npm run start:dev
```

... and a NestJS server should be running a listening on port 3004.

Point a browser to http://localhost:3004/borest/api and you should see a swagger OpenAPI page (where you can interact with the services).

