import {
    Err,
    Callback,
    errorCallback,
    debug,
    LoggedError,
    info,
    getLogger,
    LOGGER_LEVELS
} from '@glass-project1/logging';
import {KeySSI, OpenDSU, getOpenDSU, getHttpApi, getKeySSIApi, GenericCallback} from "@glass-project1/opendsu-types";
import {
    ServiceEnvironmentDefinition,
    registerGlassDIDMethod,
    EGovServiceWallet, GlassDIDSchema, HTTP_METHODS, MessagingHub
} from '@glass-project1/glass-toolkit';
import {
    BackendFileServiceImp,
    bootDisabilityReportWallet,
    generateServiceEnvironment,
    getEnvironmentFromProcess,
} from "@glass-project1/glass-toolkit/lib/providers";

import { getSeedStorage } from "@glass-project1/glass-toolkit/lib/providers/persistence";
import { EnclaveSeedStorage } from "@glass-project1/dsu-blueprint/lib/cli/persistence";
import {OpenDSURepository} from "@glass-project1/dsu-blueprint";
import {getInjectablesRegistry, injectable} from "@glass-project1/db-decorators";
import {getEGovHealthInjectables} from "./blueprints/constants";

type EndpointDef = {
    "baseEndpoint": string,
    "baseEndpointSuffix": string,
    domain?: string
}

export const COUNTRY_CODE_KEY = "GLASS_COUNTRY";
@injectable("ServiceWalletBooter")
export class ServiceWalletBooter {

    private walletCache: EGovServiceWallet;
    private storage: EnclaveSeedStorage;
    private fileService: BackendFileServiceImp;

    readonly countryCode: string;

    readonly environment: ServiceEnvironmentDefinition;

    constructor() {
        this.countryCode = (process.env[COUNTRY_CODE_KEY] || "pt").toLowerCase();
        this.environment = generateServiceEnvironment(getEnvironmentFromProcess());
        getLogger().setLevel(LOGGER_LEVELS.DEBUG)
        getEGovHealthInjectables(); // set egov-id injectables
        info.call(this,`Service wallet boot with country code: ${this.countryCode}`);
    }
    private async getEndpoints(endpoint = "egov.json"): Promise<EndpointDef>{
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const path = require('path');

        let endpoints: EndpointDef;
        try {
            endpoints = JSON.parse(fs.readFileSync(path.join(process.cwd(), "./assets/endpoints", endpoint))) as EndpointDef
        } catch (e: any) {
            throw new Error(`Failed to read ${endpoint} over: ${e.message || e}`);
        }
        return endpoints;
    }

    private getServiceInfo(environment: ServiceEnvironmentDefinition, callback: GenericCallback<KeySSI>){
        const self: ServiceWalletBooter = this;

        function getHeaders(httpMethod: HTTP_METHODS, body?: Record<string, unknown>){
            if (!body && httpMethod !== HTTP_METHODS.GET)
                throw new LoggedError("No body provided to request")

            const headers: {[indexer: string]: any} = {};
            headers['Content-Type'] = 'application/json';

            if (httpMethod !== HTTP_METHODS.GET)
                headers['Content-Length'] = `${Buffer.byteLength(JSON.stringify(body))}`;
            return headers;
        }

        self.getEndpoints("egov.json")
            .then((endpoint: EndpointDef) => {
                console.log(JSON.stringify(endpoint))
                self.fileService.getSchema((err: Err, schema?: Record<string, unknown>) => {
                    if (err || !schema)
                        return errorCallback(err || "missing schema", callback);
                    const didSchema = new GlassDIDSchema(schema);
                    const errs = didSchema.hasErrors("createdOn", "createdBy", "updatedOn", "updatedBy");
                    if (errs)
                        return errorCallback("Schema from file is invalid: {0}", callback, errs.toString());

                    debug.call(self, "Requesting DID schema: {0}", didSchema.toString())
                    const data = {
                      didSchema: schema,
                      // domain: environment.vaultDomain // está assim por conta do trafik
                      domain: `egov.${this.countryCode}`
                    }

                    debug.call(self, JSON.stringify(data));

                    const httpMethod = HTTP_METHODS.POST;
                    const headers = getHeaders(httpMethod, data)
                    const reqOptions = {
                        headers: headers,
                        method: HTTP_METHODS.POST,
                        mode: "cors",
                        body: JSON.stringify(data)
                    }

                    const url = `${endpoint.baseEndpoint}${endpoint.baseEndpointSuffix}/egov/setup/createNewServiceWallet`
                    console.log(url)
                    debug.call(self, "Calling Egov on {0} to create new service wallet with options {1}.", url, JSON.stringify(reqOptions, undefined, 2))
                    getHttpApi().fetch(url, reqOptions)
                        .then(async (response: any) => {
                            let ssi: KeySSI;
                            try {
                                response = await response.text();
                                debug.call(self, "received {0}", response);
                                ssi = getKeySSIApi().parse(response);
                            } catch (e: any){
                                return errorCallback.call(self, e, callback);
                            }
                            callback(undefined, ssi);
                        })
                        .catch((e: any) => errorCallback.call(self, e, callback))
                })
            })
            .catch((e: any) => errorCallback.call(self, e, callback));
    }

    /**
     *
     * @returns Promise
     */
    async load(): Promise<any> {
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {

            const cb = (err: Err, wallet?: EGovServiceWallet) => {
                if (err || !wallet)
                    return errorCallback.call(self, err, reject)
                resolve(wallet)
            }

            try {
                self.getOpenDSU();
            } catch (e: any) {
                return cb(e)
            }


            self.fileService = new BackendFileServiceImp(`./assets/${this.countryCode}`)

            if (self.walletCache)
                return cb(undefined, self.walletCache);

            self.initializeStorage(self.environment, (err) => {
                if(err)
                    return cb(err);

                self.storage.getCurrentKeySSI((err: Err, ssi?: KeySSI) => {
                    const promise = err || !ssi ? self.getNewServiceWallet.bind(self) : self.loadServiceWallet.bind(self)
                    promise(ssi || self.environment)
                        .then(resolve)
                        .catch(reject)
                })
            })
        })
    }


    private async getNewServiceWallet(environment: ServiceEnvironmentDefinition){
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {
            self.getServiceInfo(environment, (err: Err, walletSSI?: KeySSI) => {
                if (err || !walletSSI)
                    return errorCallback.call(self, err || "No KeySSI for the service wallet received", reject);
                self.storage.storeKeySSI(walletSSI.getIdentifier(), (err: Err) => {
                    if (err)
                        return errorCallback.call(self, err || "Failed to store the wallet SSI", reject);
                    self.loadServiceWallet(walletSSI)
                        .then(resolve)
                        .catch(reject)
                })
            })
        })
    }

    private async loadServiceWallet(ssi: string | KeySSI){
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {
            const repo = new OpenDSURepository(EGovServiceWallet);
           
            debug.call(self, typeof ssi === 'string' ? ssi : ssi.getIdentifier(true));
       
            repo.read(ssi, (err: Err, wallet?: EGovServiceWallet) => {
                if (err || !wallet)
                    return errorCallback.call(self, err || "Could not resolve service wallet", reject);
                bootDisabilityReportWallet(wallet, self.fileService, false, (err: Err, manager) => {
                    
                    debug.call(self, manager.did ? manager.did.getIdentifier() : "SEM DID NO MANAGER");

                    if (err)   
                      return errorCallback.call(self, err, reject);
                       

                    self.walletCache = wallet;


                    const messageHub = getInjectablesRegistry().get("MessageHub") as MessagingHub;

                    if (!messageHub)
                        return errorCallback.call(self, "Failed to retrieve message hub", reject);

                    messageHub.startListening();

                    resolve(wallet);
                })
            })
        })
    }

    /**
     * Utility method to require - load in runtime - OpenDSU.
     * Must be the last thing in "required" in runtime,
     * because it will break the "require" implementation.
     * @returns
     */
    private getOpenDSU() : OpenDSU {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const path = require("path");
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const opendsuUntyped = require(path.join(process.cwd(), '../privatesky/psknode/bundles/openDSU')); // delayed here due to https://gitlab.com/glass-project1/wallet/egov-id/-/issues/1#note_27486
        //console.log("OpenDSU required", opendsuUntyped);
        //warn("Accessing OpenDSU");
        const opendsu = getOpenDSU();
        registerGlassDIDMethod();
        return opendsu;
    }

    /**
     * Create new enclave storage
     *
     * @param  {any} environment
     * @param  {Callback} callback
     * @returns void
     */
    private initializeStorage(environment: any, callback: Callback): void{
        const self = this;

        const storageEnv = Object.assign({}, environment, {
          name: !!environment.name ? environment.name  : `egov.${this.countryCode}.health`,
          vaultDomain: "vault"
        });
  
        if(!self.storage)
          self.storage = getSeedStorage(storageEnv) as EnclaveSeedStorage;

        self.storage.initialize((err: Err) => {
            if(err)
                return callback(err);

            callback();
        })
    }

}
  