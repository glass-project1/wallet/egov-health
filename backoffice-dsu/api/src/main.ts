import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CatchAllFilter } from './catchall.filter';
import { getEnvironmentFromProcess } from '@glass-project1/glass-toolkit/lib/providers';
import { getLogger, info, LOGGER_LEVELS } from '@glass-project1/logging';
import { ServiceWalletBooter } from './ServiceWalletBooter';

async function bootstrap() {

  getLogger().setLevel(LOGGER_LEVELS.INFO);
  
  const env: {domain: string, didDomain: string, vaultDomain: string} = getEnvironmentFromProcess();

  const title = 'GLASS eGov Health';

  info("Backoffice DSU for {0} is running with domain {1}, didDomain {2} and vaultDomain {3}", title, env.domain, env.didDomain, env.vaultDomain);

  const walletBooter = new ServiceWalletBooter();

  await walletBooter.load()

  const PATH_PREFIX = "borest"; // /borest path prefix has x-dependencies on frontend and haproxy routing
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(PATH_PREFIX);
  app.enableCors();
  const httpAdapter = app.getHttpAdapter();
  app.useGlobalFilters(new CatchAllFilter(httpAdapter));

  // https://stackoverflow.com/questions/9153571/is-there-a-way-to-get-version-from-package-json-in-nodejs-code
  let pversion = process.env.npm_package_version;
  if (!pversion) { // when running outside of an npm run script...
    let pjson = require('../package.json');
    pversion = pjson.version;
  }

  const options = new DocumentBuilder()
    .setTitle(title)
    .setDescription('The eGov Health Backoffice API description')
    .setVersion(pversion)
    .addTag('Main')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(PATH_PREFIX + '/api', app, document);

  await app.listen(3004); // #16 egov is on 3000 ; egovid is on 3001 ; egovhealth is on 3002
}
bootstrap();
