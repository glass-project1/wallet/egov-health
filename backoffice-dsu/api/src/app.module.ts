import { Module } from '@nestjs/common'; 
import { EGovHealthModule } from './egovhealth/egovhealth.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [EGovHealthModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
