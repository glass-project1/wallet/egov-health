import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovHealth, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Health Insurance
 *
 * @class EGovHealthGr
 * @extends EvidenceBlueprint
 */
export function EGovHealthGr(injectables: ToolkitInjectables): Constructor<IEGovHealth>
export function EGovHealthGr(injectables: ToolkitInjectables, data: DataProperties<IEGovHealth>): IEGovHealth
export function EGovHealthGr(injectables: ToolkitInjectables, data?: DataProperties<IEGovHealth>): Constructor<IEGovHealth> | IEGovHealth {

    @injectables.RenderableDSUBlueprint("glass-health-insurance-gr", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-health-insurance-gr", "../components/components", undefined, false)
    class EGovHealthGr extends injectables.EvidenceBlueprint {

        @injectables.uiprop("id-number")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        id?: string = undefined;

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("insurance-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        insuranceIssueDate?: Date = undefined;

        @injectables.uiprop("insurance-expiration-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        insuranceExpirationDate?: Date = undefined;

        @injectables.uiprop("insurance-status")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        insuranceStatus?: string = undefined;

        @injectables.uiprop("birth-date")
        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        birthDate?: Date = undefined;

        @injectables.uiprop("nationality")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        nationality?: string = undefined;

        @injectables.uiprop("sex")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        gender?: string = undefined;

        constructor(EGovHealthGr?: EGovHealthGr | {}) {
            super(EGovHealthGr);
            injectables.constructFromBlueprint<EGovHealthGr>(this, EGovHealthGr);
        }
    }

    if (data)
        return new EGovHealthGr(data);

    return EGovHealthGr;
}