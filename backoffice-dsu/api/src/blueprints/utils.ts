import {Constructor} from "@glass-project1/dsu-blueprint";
import {eGovEducationInjectables} from "./constants";
import type {EGovHealthInjectables} from "./types";

type EGovHealthBlueprintFunction<T> = (injectables: EGovHealthInjectables) => Constructor<T>;

export function getBlueprint<T>(blueprintName: string, countryCode: string): EGovHealthBlueprintFunction<T> | undefined {
    countryCode = countryCode.toLowerCase();
    countryCode = countryCode.charAt(0).toUpperCase() + countryCode.slice(1);
    return eGovEducationInjectables.blueprints[blueprintName + countryCode];
}