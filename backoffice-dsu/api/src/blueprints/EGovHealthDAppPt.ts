import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovHealth, IEGovHealthDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovHealthInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovHealthDAppPt}
 *
 * Note that it contains an {@link EGovHealthDAppPt} under 'health'
 *
 * @class EGovHealthDAppPt
 * @extends DBModel
 */
export function EGovHealthDAppPt(injectables: EGovHealthInjectables): Constructor<IEGovHealthDApp>
export function EGovHealthDAppPt(injectables: EGovHealthInjectables, data: Record<string, any>): IEGovHealthDApp
export function EGovHealthDAppPt(injectables: EGovHealthInjectables, data?: Record<string, any>): Constructor<IEGovHealthDApp> | IEGovHealthDApp {

    const eGovHealthClass = injectables.blueprints.EGovHealthPt(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovHealthDAppPt extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovHealthPt, injectables, true, true)
        health?: IEGovHealth = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(EGovHealthDAppPt?: EGovHealthDAppPt | {}) {
            super();
            injectables.constructFromBlueprint<EGovHealthDAppPt>(this, EGovHealthDAppPt);
            this.did = new injectables.GlassDID(this.did);
            this.health = new eGovHealthClass(this.health);
        }
    }

    if (data)
        return new EGovHealthDAppPt(data);

    return EGovHealthDAppPt;
}