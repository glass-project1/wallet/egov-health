import type {Evidence} from "@glass-project1/dsu-blueprint";
import type {ToolkitInjectables} from "@glass-project1/glass-toolkit";
import type {EGovBirthCertificate} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovBirthCertificate";
import type {
    EGovBirthCertificateDApp
} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovBirthCertificateDApp";
import type {EGovCriminalBulletin} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovCriminalBulletin";
import type {EGovCriminalRecord} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovCriminalRecord";
import type {
    EGovCriminalRecordDApp
} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovCriminalRecordDApp";
import type {EGovEducation} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovEducation";
import type {EGovEducationDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovEducationDApp";
import type {EGovHealth} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovHealth";
import type {EGovHealthDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovHealthDApp";
import type {EGovId} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovId";
import type {EGovIdDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovIdDApp";
import type {EGovPassport} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovPassport";
import type {EGovPassportDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovPassportDApp";
import type {EGovTax} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovTax";
import type {EGovTaxDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovTaxDApp";
import type {EGovHealthDAppGr} from "./EGovHealthDAppGr";
import type {EGovHealthDAppPt} from "./EGovHealthDAppPt";
import type {EGovHealthDAppTk} from "./EGovHealthDAppTk";
import type {EGovHealthGr} from "./EGovHealthGr";
import type {EGovHealthPt} from "./EGovHealthPt";
import type {EGovHealthTk} from "./EGovHealthTk";


export interface EGovHealthInjectables extends ToolkitInjectables {
    blueprints: {
        Evidence: typeof Evidence;
        EGovId: typeof EGovId;
        EGovIdDApp: typeof EGovIdDApp;
        EGovPassport: typeof EGovPassport;
        EGovPassportDApp: typeof EGovPassportDApp;
        EGovBirthCertificate: typeof EGovBirthCertificate;
        EGovBirthCertificateDApp: typeof EGovBirthCertificateDApp;
        EGovCriminalBulletin: typeof EGovCriminalBulletin;
        EGovCriminalRecord: typeof EGovCriminalRecord;
        EGovCriminalRecordDApp: typeof EGovCriminalRecordDApp;
        EGovTax: typeof EGovTax;
        EGovTaxDApp: typeof EGovTaxDApp;
        EGovHealth: typeof EGovHealth;
        EGovHealthDApp: typeof EGovHealthDApp;
        EGovEducation: typeof EGovEducation;
        EGovEducationDApp: typeof EGovEducationDApp;

        EGovHealthGr: typeof EGovHealthGr,
        EGovHealthPt: typeof EGovHealthPt,
        EGovHealthTk: typeof EGovHealthTk,

        EGovHealthDAppGr: typeof EGovHealthDAppGr,
        EGovHealthDAppPt: typeof EGovHealthDAppPt,
        EGovHealthDAppTk: typeof EGovHealthDAppTk,
    }
}