import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovHealth, IEGovHealthDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovHealthInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovHealthDAppGr}
 *
 * Note that it contains an {@link EGovHealthDAppGr} under 'health'
 *
 * @class EGovHealthDAppGr
 * @extends DBModel
 */
export function EGovHealthDAppGr(injectables: EGovHealthInjectables): Constructor<IEGovHealthDApp>
export function EGovHealthDAppGr(injectables: EGovHealthInjectables, data: Record<string, any>): IEGovHealthDApp
export function EGovHealthDAppGr(injectables: EGovHealthInjectables, data?: Record<string, any>): Constructor<IEGovHealthDApp> | IEGovHealthDApp {

    const eGovHealthClass = injectables.blueprints.EGovHealthGr(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovHealthDAppGr extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovHealthGr, injectables, true, true)
        health?: IEGovHealth = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(EGovHealthDAppGr?: EGovHealthDAppGr | {}) {
            super();
            injectables.constructFromBlueprint(this, EGovHealthDAppGr);
            this.did = new injectables.GlassDID(this.did);
            this.health = new eGovHealthClass(this.health);
        }
    }

    if (data)
        return new EGovHealthDAppGr(data);

    return EGovHealthDAppGr;
}