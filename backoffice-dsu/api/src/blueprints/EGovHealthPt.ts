import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovHealth, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Health Insurance
 *
 * @class EGovHealthPt
 * @extends EvidenceBlueprint
 */
export function EGovHealthPt(injectables: ToolkitInjectables): Constructor<IEGovHealth>
export function EGovHealthPt(injectables: ToolkitInjectables, data: DataProperties<IEGovHealth>): IEGovHealth
export function EGovHealthPt(injectables: ToolkitInjectables, data?: DataProperties<IEGovHealth>): Constructor<IEGovHealth> | IEGovHealth {

    @injectables.RenderableDSUBlueprint("glass-health-insurance-pt", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-health-insurance-pt", "../components/components", undefined, false)
    class EGovHealthPt extends injectables.EvidenceBlueprint {

        @injectables.uiprop("id-number")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        id?: string = undefined;

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("insurance-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        insuranceIssueDate?: Date = undefined;

        @injectables.uiprop("insurance-expiration-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        insuranceExpirationDate?: Date = undefined;

        @injectables.uiprop("insurance-status")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        insuranceStatus?: string = undefined;

        @injectables.uiprop("birth-date")
        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        birthDate?: Date = undefined;

        @injectables.uiprop("nationality")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        nationality?: string = undefined;

        @injectables.uiprop("sex")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        gender?: string = undefined;

        constructor(EGovHealthPt?: EGovHealthPt | {}) {
            super(EGovHealthPt);
            injectables.constructFromBlueprint<EGovHealthPt>(this, EGovHealthPt);
        }
    }

    if (data)
        return new EGovHealthPt(data);

    return EGovHealthPt;
}