import {getBlueprintInjectables, setBlueprintInjectables} from "@glass-project1/dsu-blueprint";
import {toolkitInjectables} from "@glass-project1/glass-toolkit";
import {EGovHealthDAppGr} from "./EGovHealthDAppGr";
import {EGovHealthDAppPt} from "./EGovHealthDAppPt";
import {EGovHealthDAppTk} from "./EGovHealthDAppTk";
import {EGovHealthGr} from "./EGovHealthGr";
import {EGovHealthPt} from "./EGovHealthPt";
import {EGovHealthTk} from "./EGovHealthTk";

import type {EGovHealthInjectables} from "./types";

export const eGovEducationInjectables: EGovHealthInjectables = {
    ...toolkitInjectables,
    blueprints: {
        ...toolkitInjectables.blueprints,

        EGovHealthGr,
        EGovHealthPt,
        EGovHealthTk,

        EGovHealthDAppGr,
        EGovHealthDAppPt,
        EGovHealthDAppTk,
    }
}

export function getEGovHealthInjectables(): EGovHealthInjectables {
    setBlueprintInjectables(eGovEducationInjectables, true);
    return getBlueprintInjectables() as EGovHealthInjectables;
}