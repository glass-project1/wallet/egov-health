import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovHealth, IEGovHealthDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovHealthInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovHealthDAppTk}
 *
 * Note that it contains an {@link EGovHealthDAppTk} under 'health'
 *
 * @class EGovHealthDAppTk
 * @extends DBModel
 */
export function EGovHealthDAppTk(injectables: EGovHealthInjectables): Constructor<IEGovHealthDApp>
export function EGovHealthDAppTk(injectables: EGovHealthInjectables, data: Record<string, any>): IEGovHealthDApp
export function EGovHealthDAppTk(injectables: EGovHealthInjectables, data?: Record<string, any>): Constructor<IEGovHealthDApp> | IEGovHealthDApp {

    const eGovHealthClass = injectables.blueprints.EGovHealthTk(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovHealthDAppTk extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovHealthTk, injectables, true, true)
        health?: IEGovHealth = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(EGovHealthDAppTk?: EGovHealthDAppTk | {}) {
            super();
            injectables.constructFromBlueprint<EGovHealthDAppTk>(this, EGovHealthDAppTk);
            this.did = new injectables.GlassDID(this.did);
            this.health = new eGovHealthClass(this.health);
        }
    }

    if (data)
        return new EGovHealthDAppTk(data);

    return EGovHealthDAppTk;
}