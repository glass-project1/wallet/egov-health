import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common"
import { IsDateString, IsEnum, IsInt, IsNumber, IsOptional, IsString, Min, validate } from "class-validator"
import { plainToClass, Transform } from "class-transformer"
import { ApiProperty } from "@nestjs/swagger"

@Injectable()
export class EGovHealthQueryValidator implements PipeTransform<EGovHealthQuery> {
    async transform(value: object, metadata: ArgumentMetadata): Promise<EGovHealthQuery> {
        console.log('EGovHealthQuery.validator.transform raw=', value)
        const search = plainToClass(metadata.metatype, value)
        const errors = await validate(search, { skipMissingProperties: false, whitelist: true, transform: true })
        if (errors.length > 0) {
            const message = Object.values(errors[0].constraints).join(". ").trim()
            throw new BadRequestException(message)
        }
        console.log('EGovHealthQuery.validator.transform return=', search)
        return search
    }
}

export enum EGovHealthQuerySortProperty {
    ID = "id",
    GIVEN_NAME = "givenName",
    SURNAME = "surname"
};
export enum EGovHealthQuerySortDirection {
    ASC = "ASC",
    DESC = "DESC"
};
export class EGovHealthQuery {

    @ApiProperty({ required: false, description: "Filter by exact match to EGovHealth.id" })
    @IsOptional()
    @IsString({ each: true })
    id: string;

    @ApiProperty({ required: false, description: "Filter by substring match to EGovHealth.givenName" })
    @IsOptional()
    @IsString({ each: true })
    givenName: string;

    @ApiProperty({ required: false, description: "Filter by substring match to EGovHealth.surname" })
    @IsOptional()
    @IsString({ each: true })
    surname: string;

    @ApiProperty({ required: false, description: "Number of items per page. Defaults to 10." })
    @IsOptional()
    @IsInt()
    @Min(1)
    @Transform(({ value }) => parseInt(value))
    limit: number = 10;

    @ApiProperty({ required: false, description: "Page number. Starts at zero. Defaults to zero." })
    @IsOptional()
    @IsInt()
    @Min(0)
    @Transform(({ value }) => parseInt(value))
    page: number = 0;

    @ApiProperty({ required: false, description: "Sort property name. Defaults to 'givenName'. Possible values are 'id', 'givenName', 'surname'." })
    @IsOptional()
    @IsEnum(EGovHealthQuerySortProperty, { each: true })
    sortProperty: EGovHealthQuerySortProperty = EGovHealthQuerySortProperty.ID;

    @ApiProperty({ required: false, description: "Sort property order. Use ASC or DESC. Defaults to ASC." })
    @IsOptional()
    @Transform(({ value }) => {
        return value.toUpperCase()
    })
    @IsEnum(EGovHealthQuerySortDirection, { each: true })
    sortDirection: EGovHealthQuerySortDirection = EGovHealthQuerySortDirection.ASC;

};