import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppResourceController } from './appresource.controller';
import { EgovHealthController } from './egovhealth.controller';
import { EGovHealthService } from './egovhealth.service';
import { AppUserService } from './appuser.service';
import { DisabilityReportController } from './disabilityreport.controller';
import { DisabilityReportService } from "./disabilityreport.service";
import { DisabilityReportConvertController } from './disabilityreportconvert.controller';
import { DisabilityReportConvertService } from './disabilityreportconvert.service';
import { HealthInsuranceController } from './healthinsurance.controller';
import { HealthInsuranceService } from './healthinsurance.service';

@Module({
  imports: [HttpModule,
    TypeOrmModule.forRoot({
    "name": "default",
    "type": "postgres",
    "host": ( process.env.EGOVHEALTHDB_HOST || "localhost" ),
    "port": ( process.env.EGOVHEALTHDB_PORT ? parseInt(process.env.EGOVHEALTHDB_PORT) : 5432 ),
    "username": "egovhealth",
    "password": "egovhealth",
    "database": "egovhealth",
    "entities": [
      "dist/egovhealth/*.entity.js"
    ],
    "synchronize": false,
    "logging": true
  })],
  controllers: [
    AppResourceController,
    EgovHealthController,
    DisabilityReportController,
    DisabilityReportConvertController,
    HealthInsuranceController
  ],
  providers: [
    EGovHealthService,
    AppUserService,
    DisabilityReportService,
    DisabilityReportConvertService,
    HealthInsuranceService
  ],
  exports: [
    AppUserService
  ],
})
export class EGovHealthModule { }
