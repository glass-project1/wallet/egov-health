import {Connection} from "typeorm";
import {Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards} from "@nestjs/common";
import {ApiBearerAuth, ApiOperation, ApiTags} from "@nestjs/swagger";
import {AppUser} from "./appuser.entity";
import {AuthGuard} from "@nestjs/passport";

@ApiTags("AppUser")
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller("/egovhealth/appuser")
export class AppUserController {
    constructor(private connection: Connection) {
    }

    @Get()
    @ApiOperation({summary: "Get all AppUser"})
    async findAll(@Query() query: AppUser): Promise<AppUser[]> {
        let appUserCollection = await AppUser.find({order: {userId: "ASC"}});
        console.log("appUser.findAll, appUserColletion =", appUserCollection);
        return appUserCollection;
    }

    @Put(":id")
    @ApiOperation({summary: "Update one AppUser"})
    async update(@Param("id") id: string, @Body() appUser: AppUser): Promise<AppUser> {
        console.log("appUser.update... appUser=", appUser);
        const appUserRepository = this.connection.getRepository(AppUser);
        await appUserRepository.save(appUser);
        console.log("appUser.update, appUser =", appUser);
        return appUser;
    }

    @Post()
    @ApiOperation({summary: "Create one AppUser"})
    async create(@Body() appUser: AppUser): Promise<AppUser> {
        console.log("appUser.create... appUser=", appUser);
        const appUserRepository = this.connection.getRepository(AppUser);
        await appUserRepository.save(AppUser);
        console.log("appUser.create, appUser =", appUser);
        return appUser;
    }

    @Get(":id")
    @ApiOperation({summary: "Get one AppUser"})
    async findOne(@Param("id") id: string): Promise<AppUser> {
        console.log("appUser.findOne... id=", id);
        let appUser = await AppUser.findOne(id);
        console.log("appUser.findOne appUser =", appUser);
        return appUser;
    }

    @Delete(":id")
    @ApiOperation({summary: "Delete one AppUser"})
    async remove(@Param("id") id: string): Promise<void> {
        console.log("appUser.delete... id=", id);
        const appUserRepository = this.connection.getRepository(AppUser);
        const delResult = await appUserRepository.delete(id);
        console.log("appUser.delete, egovidUser =", delResult.raw);
        return;
    }
}
