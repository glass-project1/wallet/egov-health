import { Connection } from "typeorm";
import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiOkResponse } from "@nestjs/swagger";
import { EGovHealthEntity } from './egovhealth.entity';
import { EGovHealthService } from './egovhealth.service';
import { EGovHealthQuery, EGovHealthQueryValidator } from "./egovhealthquery.validator";
import { EGovHealthRepository } from "./egovhealth.repository";
import { PaginatedDto } from "../paginated.dto";



 
@ApiExtraModels(PaginatedDto)
@ApiTags('EgovHealth')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovhealth/egovhealth')
export class EgovHealthController {

    private eGovHealthRepository: EGovHealthRepository;

    constructor(
        private connection: Connection,
        private egovHealthService: EGovHealthService
    ) {
        this.eGovHealthRepository = this.connection.getCustomRepository(EGovHealthRepository);
    }

    @Get()
    @ApiOperation({summary: "Search for EGovHealth based on a query, with paginated results."})
    @ApiOkResponse({
        schema: {
            allOf: [
                { $ref: getSchemaPath(PaginatedDto) },
                {
                    properties: {
                        results: {
                            type: 'array',
                            items: { $ref: getSchemaPath(EGovHealthEntity) },
                        },
                    },
                },
            ],
        },
    })
    async search(@Query(EGovHealthQueryValidator) eGovHealthQuery: EGovHealthQuery): Promise<PaginatedDto<EGovHealthQuery, EGovHealthEntity>> {
        console.log("egovhealth.controller.search... query=", eGovHealthQuery);
        const page = await this.eGovHealthRepository.search(eGovHealthQuery);
        console.log("egovhealth.controller.search results =", page);
        return page;
    }

    @Get(":id")
    @ApiOperation({ summary: 'Get one EGovHealth record' })
    @ApiParam({ name: 'id', type: String, description: "Exact match of primary key column EGovHealth.id" })
    @ApiOkResponse({
        type: EGovHealthEntity
    })
    async findOne(@Param() params): Promise<EGovHealthEntity> {
        console.log("egovhealth.controller.findOne... id=", params.id);
        let eGovHealth = await EGovHealthEntity.findOne(params.id);
        if (!eGovHealth) throw new NotFoundException(`Not found EGovHealth.id="${params.id}"`);
        console.log("egovhealth.controller.findOne egovhealth =", eGovHealth);
        return eGovHealth;
    }

    /* don't allow  update PUT */

    @Post(":id")
    @ApiOperation({ summary: 'Create one wallet for a given ID' })
    @ApiParam({ name: 'id', type: String })
    @ApiOkResponse({
        status: 201,
        type: EGovHealthEntity
    })
    async createWallet(@Param() params, @Body() body: any): Promise<EGovHealthEntity> {
        console.log(`egovhealth.controller.post/${params.id}...body`, body);

        const eGovHealth = await this.egovHealthService.createWallet(params.id);
    
        console.log("egovhealth.controller.post/${params.id} DB connection closed, egovhealth =", eGovHealth);
        return eGovHealth;
    }


    @Get("/setup/onetime")
    @ApiOperation({ summary: 'Setup a wallet with a DID for this government agency. No need to invoke explicitely, as it will be invoked internally on the 1st use.' })
    @ApiOkResponse({ description: "The returned string is the agency's DID. (Even if invoked more than once, the agency DID is always the same)."})
    async setup() : Promise<string> {
        console.log(`egovhealth.controller.setup`);

        const agencyDid = await this.egovHealthService.getAgencyDID();
    
        console.log("egovhealth.controller.setup =", agencyDid);
        return agencyDid.getIdentifier();
    }

}

