import { Controller, Get, Put, Param, Body, Post} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, ApiResponse } from "@nestjs/swagger";
import { HealthInsuranceService } from './healthinsurance.service';




@ApiTags('HealthInsurance')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovhealth/healthinsurance')
export class HealthInsuranceController {

    constructor(
        private DisabilityService: HealthInsuranceService
    ) {
    }

    @Post(":did")
    @ApiOperation({summary: 'Create one GlassEvidenceRequest for a given receiverDid'})
    @ApiParam({name: 'did', type: String, description: "Receiver DID"})
    @ApiResponse({ status: 201, description: 'The GlassEvidenceRequest object created.'})
    async create(@Param() params, @Body() body: any): Promise<any> {
        console.log(`disabilityreportconvert.controller.post/${params.did}...body`, body);

        const result = await this.DisabilityService.create(params.did);

        console.log(`Disability.controller.post/${params.did} = ${result}`);
        return result;
    }
}
