import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiResponse } from "@nestjs/swagger";
import { DisabilityReportConvertService } from './disabilityreportconvert.service';




@ApiTags('DisabilityReportConvert')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovhealth/disabilityreportconvert')
export class DisabilityReportConvertController {

    constructor(
        private DisabilityService: DisabilityReportConvertService
    ) {
    }

    @Post(":did")
    @ApiOperation({summary: 'Create one GlassEvidenceRequest for a given receiverDid'})
    @ApiParam({name: 'did', type: String, description: "Receiver DID"})
    @ApiResponse({ status: 201, description: 'The GlassEvidenceRequest object created.'})
    async create(@Param() params, @Body() body: any): Promise<any> {
        console.log(`disabilityreportconvert.controller.post/${params.did}...body`, body);

        const result = await this.DisabilityService.create(params.did);

        console.log(`Disability.controller.post/${params.did} = ${result}`);
        return result;
    }
}
