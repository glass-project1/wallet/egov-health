import { Injectable, NotFoundException } from '@nestjs/common';
import { EGovHealthService } from './egovhealth.service';
import {GlassEvidenceRequest, GlassEvidenceShared, GlassWalletManager} from "@glass-project1/glass-toolkit";
import {InternalServerError, DisabilityReportActions} from "@glass-project1/glass-toolkit/lib/providers";
import { DSUDid } from "@glass-project1/opendsu-types";
import {getInjectablesRegistry, inject} from "@glass-project1/db-decorators";
import {LocaleServiceInterface} from "@glass-project1/localization";
import { ServiceWalletBooter } from 'src/ServiceWalletBooter';

@Injectable()
export class DisabilityReportConvertService {

    @inject("GlassWalletManager")
    walletManager!: GlassWalletManager;

    @inject("ServiceWalletBooter")
    walletBooter!: ServiceWalletBooter;

    constructor(
        private egovHealthService: EGovHealthService,
    ) {
    }

    /**
     *
     */
    async create(receiverDid: string): Promise<any> {
        const self = this;
        const debugId = "Disability create "+receiverDid;

        let resultResolve;
        let resultReject;
        const result = new Promise<any>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        result.catch(async (err) => {
            // self.egovHealthService.releaseOpenDSU(openDsuLockedFlag, debugId);
            //throw new Error(err); // do not (re) throw, or nestjs will die. Rejection is enough.
            //console.log(`Rejected with ${err}`);
        });
        try {
            const gwm = this.walletManager

            const locale = getInjectablesRegistry().get("GlassLocaleService") as LocaleServiceInterface
            // #24 based on test https://gitlab.com/glass-project1/wallet/glass-toolkit/-/blob/00b27c21ab70da5dcbbee86022d8c6d2b284c6d3/tests/environment/environment.MoJ.test.ts#L79

            const evidenceRequest = new GlassEvidenceRequest({
              requestId: Date.now().toString(),
              purpose: locale.get("purposes.issue.disability-report") as string,
              action: DisabilityReportActions.CONVERT_DISABILITY_REPORT,
              requesterDID:  (gwm.did as DSUDid).getIdentifier(),
              receiverDID: receiverDid,
              payload: [`egov.*.moj.id.*`, `egov.*.moh.disability.*`, `egov.${self.walletBooter.countryCode}.moh.health.*`, `egov.${self.walletBooter.countryCode}.ibs.permits.residence.*`]
            })  
            
            const res = await gwm.handleRequest(evidenceRequest);
            if (!res) {
                resultReject("Missing result on issue disability report");
                return result;
            }
            resultResolve(res);
        } catch (err) {
            resultReject(err);
        }

        return result;
    }

}

