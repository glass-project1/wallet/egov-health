import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';


@Entity("egovhealth")
export class EGovHealthEntity extends BaseEntity {  

    @ApiProperty({ description: "CCV_00022_IDnumber Mandatory Citizen Identification Number" })
    @PrimaryColumn()
    id: string;  

    @ApiProperty({ description: "CCV_00002_name Given names" })
    @Column({ name: "givenname" })
    givenName: string;

    @ApiProperty({ description: "CCV_00003_Surname Surnames" })
    @Column()
    surname: string;

    @ApiProperty({ description: "CCV_00042_insuranceIssueDate Health insurance issue date" })
    @Column({ name: "insuranceissuedate" })
    insuranceIssueDate: string;
      
    @ApiProperty({ description: "CCV_00043_insuranceExpirationDate Health insurance expiration date" })
    @Column({ name: "insuranceexpirationdate" })
    insuranceExpirationDate: string;

    @ApiProperty({ description: "CCV_00083_insuranceStatus Insurance status" })
    @Column({ name: "insurancestatus" })
    insuranceStatus: string;

    @ApiProperty({ description: "CCV_00004_DateofBirth Birth date" })
    @Column({ name: "birthdate" })
    birthDate: Date;

    @ApiProperty({ description: "CCV_00012_Nationality Nationality" })
    @Column({ name: "nationality" })
    nationality: string;

    @ApiProperty({ description: "CCV_00013_Sex Gender" })
    @Column({ name: "gender" })
    gender: string;

    @ApiProperty({ description: "A JSON containing the wallet DSU seed keySSI. Non-null if the wallet has already been created. See AppResource.key=egovhealth.dapp.keyTemplate for an example of such JSON.", required: false })
    @Column({ name: "walletkeyssi" })
    walletKeySSI: string;

    @ApiProperty({ description: "ISO-3166-1 alpha-2 two letter country code to which this record belongs. Normally, all the records in the same agency belong to the same country, but this column exists for developers to be allowed to mix records from distinct countries.", required: false })
    @Column({ name: "country" })
    country: string;
}
