import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AppUser } from '../egovhealth/appuser.entity';
import { AppUserService } from '../egovhealth/appuser.service';

@Injectable()
export class AuthService {
    constructor(
        private appUserService: AppUserService,
        private jwtService: JwtService
    ) { }

    /**
     * Validate a username/password.
     * @param appuserUsername
     * @param appuserPassHash password in clear text.
     * @returns an AppUser if matched. null if not matched.
     */
    async validateUser(appuserUsername: string, appuserPassHash: string): Promise<AppUser> {
        console.log("AuthService.validateUser ", appuserUsername, appuserPassHash);
        if (!appuserUsername) {
            console.log("AuthService.validateUser returned null because of missing username");
            return null;
        }
        const appUserCollection = await this.appUserService.findByEmail(appuserUsername);
        console.log("AuthService.validateUser found ", appUserCollection);
        if (!appUserCollection || appUserCollection.length == 0) {
            console.log("AuthService.validateUser returned null because username not found!");
            return null;
        }
        if (appUserCollection[0].passHash === appuserPassHash // TODO clear text comparison to bcrypt
        ) {
            console.log("AuthService.validateUser returned ", appUserCollection[0]);
            return appUserCollection[0];
        }
        console.log("AuthService.validateUser returned null");
        return null;
    }

    /**
     * Transforms a valid AppUser into a valid (signed) JWT token.
     * @param appUser an AppUser object, as returned by LocalStrategy.validate()
     * @returns an object with the JWT authentication token. Please document the return type in the auth.controller.ts login method
     */
    async login(appUser: AppUser) {
        const payload = {userId: appUser.userId, email: appUser.email};
        return {
            userId: appUser.userId,
            email: appUser.email,
            token: this.jwtService.sign(payload),
        };
    }

    /**
     * Marks this JWT token as expired.
     * @param au an EGovIdUser object, as returned by JwtStrategy.validate()
     * @returns an object with the JWT authentication token.
     */
    async logout(au: any) {
        // TODO
        return au;
    }
}
