import {Component, h, getAssetPath, Prop} from '@stencil/core';


@Component({
  tag: 'glass-health-insurance',
  styleUrl: 'glass-health-insurance.scss',
  shadow: true,
})
export class GlassHealthInsurance {

  /**
   * Controls if the card is flipped or not (true means shown the back part)
   */
  @Prop({attribute: "flip-card", mutable: true, reflect: true}) isFlipped: boolean = false;


  /**
  * The location of the portuguese brand
  */
  @Prop({attribute: "src-brand"}) scrBrand: string = "portuguese-republic.png";


  /**
  * The id-number of the id
  */
 @Prop({attribute: "id-number"}) idNumber: string = "XX0980320456908";

  /**
  * The givenname of the id
  */
  @Prop({attribute: "given-name"}) givenName: string = "Ana Cardoso";

  /**
  * The surname of the id
  */
  @Prop({attribute: "surname"}) surname: string = "dos Santos Barbosa";

  /**
  * Insurance Issue date
  */

  @Prop({attribute: "insurance-issue-date"}) insuranceIssueDate: Date | string = "2023/04/12";

  /**
  * Insurance Expiration date
  */

  @Prop({attribute: "insurance-expiration-date"}) insuranceExpirationdate: Date | string = "2024/04/12";

  /**
  * The sex of the id
  */
  @Prop({attribute: "insurance-status"}) insuranceStatus: string = "Active";

  /**
  * Insurance Expiration date
  */
  @Prop({attribute: "birth-date"}) birthDate: string = "1990/01/01";

  /**
  * Nationality
  */
  @Prop({attribute: "nationality"}) nationality: string = "Portugal"

  /**
  * The sex
  */
  @Prop({attribute: "gender"}) gender: string = "Female"



 componentDidLoad(){}

  private parseDate(date: Date | string){
    return new Date(date).toLocaleString('pt-PT', {dateStyle: 'medium'});
  }


 /**
  * Flips the card. eg: changes the {@link flipped} property
  */
 flip() {
  this.isFlipped = !this.isFlipped;
 }


 render() {
  const self: GlassHealthInsurance = this;

   return (
     <div class="g-ssc">
       <div class={{ 'g-ssc-inner': true, 'g-flipped-card': this.isFlipped }}>
         <div class="g-ssc-card-front">
           <ion-grid class="g-ssc-header">
             <ion-row>
               <ion-col>
                 <div class="g-ssc-header-wrapper">
                   <div class="g-ssc-header-inner">
                     <ion-text>
                       <h2>Plano de Saúde</h2>
                       <p>Health Insurance</p>
                     </ion-text>
                   </div>
                   <ion-button onClick={self.flip.bind(self)} color="none">
                     <ion-icon name="repeat-outline"></ion-icon>
                   </ion-button>
                 </div>
               </ion-col>
             </ion-row>
           </ion-grid>
           <ion-grid class="g-ssc-content">
             <ion-row>
               <ion-col>
                 <h2>Nome</h2>
                 <h3>Given Name</h3>
                 <p>{self.givenName}</p>
               </ion-col>
               <ion-col>
                 <h2>Sobrenome</h2>
                 <h3>Surname</h3>
                 <p>{self.surname}</p>
               </ion-col>
             </ion-row>
             <ion-row>
               <ion-col>
                 <h2>Data de Emissão</h2>
                 <h3>Issuing Date</h3>
                 <p>{self.parseDate(self.insuranceIssueDate)}</p>
               </ion-col>
               <ion-col>
                 <h2>Data de Validade</h2>
                 <h3>Expiration Date</h3>
                 <p>{self.parseDate(self.insuranceExpirationdate)}</p>
               </ion-col>
             </ion-row>
           </ion-grid>
         </div>
         <div class="g-ssc-card-back">
           <ion-grid class="g-ssc-header">
             <ion-row>
               <ion-col>
                 <div class="g-ssc-header-wrapper">
                   <div class="g-ssc-header-inner">
                     <ion-img src={getAssetPath(`../assets/images/${self.scrBrand}`)}></ion-img>
                   </div>
                   <ion-button onClick={() => this.flip()} color="none">
                     <ion-icon name="repeat-outline"></ion-icon>
                   </ion-button>
                 </div>
               </ion-col>
             </ion-row>
           </ion-grid>
           <ion-grid class="g-ssc-content">
             <ion-row>
               <ion-col>
                 <h2>Nacionalidade</h2>
                 <h3>Nationality</h3>
                 <p>{self.nationality}</p>
               </ion-col>
               <ion-col>
                 <h2>Nascimento</h2>
                 <h3>Birth</h3>
                 <p>{self.parseDate(self.birthDate)}</p>
               </ion-col>
               <ion-col>
                <div class="g-scc-number">
                  <div>
                    <h2>Número</h2>
                    <h3>Number</h3>
                  </div>
                  <div>
                    <p>{self.idNumber}</p>
                  </div>
                </div>
               </ion-col>
             </ion-row>
           </ion-grid>
         </div>
       </div>
   </div>
   );
 }

}
