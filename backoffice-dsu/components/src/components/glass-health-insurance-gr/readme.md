# glass-health-insurance-gr



<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute                   | Description                                                             | Type             | Default                |
| ------------------------- | --------------------------- | ----------------------------------------------------------------------- | ---------------- | ---------------------- |
| `birthDate`               | `birth-date`                | Insurance Expiration date                                               | `string`         | `"1990/01/01"`         |
| `gender`                  | `gender`                    | The sex                                                                 | `string`         | `"Female"`             |
| `givenName`               | `given-name`                | The givenname of the id                                                 | `string`         | `"Ana Cardoso"`        |
| `idNumber`                | `id-number`                 | The id-number of the id                                                 | `string`         | `"XX0980320456908"`    |
| `insuranceExpirationdate` | `insurance-expiration-date` | Insurance Expiration date                                               | `Date \| string` | `"2024/04/12"`         |
| `insuranceIssueDate`      | `insurance-issue-date`      | Insurance Issue date                                                    | `Date \| string` | `"2023/04/12"`         |
| `insuranceStatus`         | `insurance-status`          | The sex of the id                                                       | `string`         | `"Active"`             |
| `isFlipped`               | `flip-card`                 | Controls if the card is flipped or not (true means shown the back part) | `boolean`        | `false`                |
| `nationality`             | `nationality`               | Nationality                                                             | `string`         | `"Portugal"`           |
| `scrBrand`                | `src-brand`                 | The location of the portuguese brand                                    | `string`         | `"greek-republic.png"` |
| `surname`                 | `surname`                   | The surname of the id                                                   | `string`         | `"dos Santos Barbosa"` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
