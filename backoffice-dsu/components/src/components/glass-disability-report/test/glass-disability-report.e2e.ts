import { newE2EPage } from '@stencil/core/testing';

describe('glass-disability-report', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-disability-report></glass-disability-report>');

    const element = await page.find('glass-disability-report');
    expect(element).toHaveClass('hydrated');
  });
});
