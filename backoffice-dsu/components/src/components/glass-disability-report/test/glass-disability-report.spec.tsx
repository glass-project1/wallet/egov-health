import { newSpecPage } from '@stencil/core/testing';
import { GlassDisabilityReport } from '../glass-disability-report';

describe('glass-disability-report', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassDisabilityReport],
      html: `<glass-disability-report></glass-disability-report>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-disability-report>
        <mock:shadow-root>
        <div class="g-evidence-details">
            <ion-grid>
              <ion-row>
                <ion-col class="g-evidence-details-header">
                  <div class="g-evidence-details-icon">
                    <ion-icon name="checkmark-outline"></ion-icon>
                  </div>
                  <ion-text>
                    <h1>Disability Report</h1>
                    <p>The following data has been successfully confirmed.</p>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col class="g-evidence-details-content">
                  <ion-list lines="none">

                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Customer</h2>
                        <p>Alice</p>
                      </ion-label>
                    </ion-item>

                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Total Value</h2>
                        <p>0</p>
                      </ion-label>
                    </ion-item>
                    
                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Issuing Authority</h2>
                        <p>SEF</p>
                      </ion-label>
                    </ion-item>

                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Issuing Date</h2>
                        <p>2023/02/07</p>
                      </ion-label>
                    </ion-item>
                    
                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Expiration Date</h2>
                        <p>2023/03/07</p>
                      </ion-label>
                    </ion-item>

                    <ion-item class="g-record-items">
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>Records</h2>
                        <p>No informed data</p>
                      </ion-label>
                    </ion-item>

                  </ion-list>
                </ion-col>
              </ion-row>
            </ion-grid>
          </div>
        </mock:shadow-root>
      </glass-disability-report>
    `);
  });
});
