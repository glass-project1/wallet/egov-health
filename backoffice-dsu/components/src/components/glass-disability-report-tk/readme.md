# glass-disability-report-tk



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description               | Type     | Default            |
| ------------------ | ------------------- | ------------------------- | -------- | ------------------ |
| `expirationDate`   | `expiration-date`   | Expiration date of report | `string` | `"2023/03/07"`     |
| `givenName`        | `given-name`        | The givenname of the id   | `string` | `"Alice"`          |
| `idNumber`         | `id-number`         | The id-number of the id   | `string` | `"123456789"`      |
| `issuingAuthority` | `issuing-authority` | Issuing Authority name    | `string` | `"SEF"`            |
| `issuingDate`      | `issuing-date`      | Issuing date of report    | `string` | `"2023/02/07"`     |
| `records`          | `records`           | Records with disabilitys  | `any`    | `undefined`        |
| `surname`          | `surname`           | The surname of the id     | `string` | `"dolor sit amet"` |
| `totalValue`       | `total-value`       | Total of disability       | `string` | `"0"`              |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
