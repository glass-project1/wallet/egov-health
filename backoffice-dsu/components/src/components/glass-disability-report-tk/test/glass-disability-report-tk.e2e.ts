import { newE2EPage } from '@stencil/core/testing';

describe('glass-disability-report-tk', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-disability-report-tk></glass-disability-report-tk>');

    const element = await page.find('glass-disability-report-tk');
    expect(element).toHaveClass('hydrated');
  });
});
