import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'glass-disability-report-tk',
  styleUrl: 'glass-disability-report-tk.scss',
  shadow: true,
})

export class GlassDisabilityReportTk {
  /**
   * The id-number of the id
   */
  @Prop({attribute: "id-number"}) idNumber: string = "123456789";

  /**
   * The givenname of the id
   */
  @Prop({attribute: "given-name"}) givenName: string = "Alice";

  /**
   * The surname of the id
   */
  @Prop({attribute: "surname"}) surname: string = "dolor sit amet";

  /**
   * Total of disability
   */
  @Prop({attribute: "total-value"}) totalValue: string = "0";

  /**
   * Issuing Authority name
   */
  @Prop({attribute: "issuing-authority"}) issuingAuthority: string = "SEF";

  /**
   * Issuing date of report
   */
  @Prop({attribute: "issuing-date"}) issuingDate: string = "2023/02/07";

  /**
   * Expiration date of report
   */
  @Prop({attribute: "expiration-date"}) expirationDate: string = "2023/03/07";

  /**
   * Records with disabilitys
   */
  @Prop({attribute: "records"}) records: any = undefined;


  private getRecords() {

    let records: any = !!this.records ? JSON.parse(this.records as string) : this.records;

    if(!records || records.length == 0)
      return ("Bilgilendirilmiş veri yok");

    records = records.map((record: {[indexer: string]: any}) => {
      return (
        <div class="g-item">
          <ion-text>
            <ion-icon name="information-circle-outline"></ion-icon>
            {record.name} - Değer: {record.value}
          </ion-text>
        </div>
      );
    });

    return (<div>{...records}</div>);

  }

  render() {
    const self = this;

    return (
      <div class="g-evidence-details">
        <ion-grid>
          <ion-row>
            <ion-col class="g-evidence-details-header">
              <div class="g-evidence-details-icon">
                <ion-icon name="checkmark-outline"></ion-icon>
              </div>
              <ion-text>
                <h1>Engelli Raporu</h1>
                <p>Aşağıdaki veriler başarıyla onaylandı.</p>
              </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col class="g-evidence-details-content">
              <ion-list lines="none">

                <ion-item>
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Müşteri</h2>
                    <p>{self.givenName}</p>
                  </ion-label>
                </ion-item>

                <ion-item>
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Toplam değer</h2>
                    <p>{self.totalValue}</p>
                  </ion-label>
                </ion-item>

                <ion-item>
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Düzenleyen yetkili makam</h2>
                    <p>{self.issuingAuthority}</p>
                  </ion-label>
                </ion-item>

                <ion-item>
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Veriliş tarihi</h2>
                    <p>{self.issuingDate}</p>
                  </ion-label>
                </ion-item>

                <ion-item>
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Son kullanma tarihi</h2>
                    <p>{self.expirationDate}</p>
                  </ion-label>
                </ion-item>

                <ion-item class="g-record-items">
                  <ion-icon name="reader-outline" slot="start"></ion-icon>
                  <ion-label>
                    <h2>Kayıtlar</h2>
                    <p>{this.getRecords()}</p>
                  </ion-label>
                </ion-item>

              </ion-list>
            </ion-col>
          </ion-row>
        </ion-grid>
      </div>
    );
  }

}
