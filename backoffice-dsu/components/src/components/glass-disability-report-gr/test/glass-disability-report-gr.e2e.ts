import { newE2EPage } from '@stencil/core/testing';

describe('glass-disability-report-gr', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-disability-report-gr></glass-disability-report-gr>');

    const element = await page.find('glass-disability-report-gr');
    expect(element).toHaveClass('hydrated');
  });
});
