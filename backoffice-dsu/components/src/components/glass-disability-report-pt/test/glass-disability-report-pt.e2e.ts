import { newE2EPage } from '@stencil/core/testing';

describe('glass-disability-report-pt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-disability-report-pt></glass-disability-report-pt>');

    const element = await page.find('glass-disability-report-pt');
    expect(element).toHaveClass('hydrated');
  });
});
