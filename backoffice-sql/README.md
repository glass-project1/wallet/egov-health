# backoffice-sql

This folder has a "legacy" RDBMS (SQL) database to encapsulate the "master" eGovernment health data representation, and also to record issued GLASS eGov-health wallets.

To avoid duplication of instructions, the SQL setup 
instructions are the same as for the eGov-id,
but instead of setting up a database called "egovid", they setup a database called "egovhealth".

## Evaluation

For evaluation purposes, just read the parent's directory README.md.


## Setup local PostgreSQL database

For developers that need to make a change to this database, read this section to setup a development environment.

### Install PostgreSQL client+server+contrib+libpq

You will probably need a native PostgreSQL installation. These instructions are for a debian derived distro (such as Ubuntu) - but they could be adapted to any other environment supporting PostgreSQL.

On a Linux (debian based release), read PROJECT-ROOT/backoffice-sql/lib/sql/install/setup.sh
(adapt it with another name, if needed), and execute it as root.
On Windows, please see download and installation instructions on http://www.postgresql.org

This will install PostgreSQL, client and server (including the native libpq client).

PostgreSQL version >= 12.9

(It should work fine on any version above 12.9, but please contact the team before changing the PROJECT-ROOT/backoffice-sql/lib/sql/install/egovhealth.sql file to support a more recent version only.)


### Instructions to create a new PostgreSQL egov-health database

As of now, you should have a local PostgreSQL server running.
(These instructions assume a Linux environment, but a Windows environment
will be equivalent: without the sudo command ; path-name slashes "/" changed for "\" ;
the psql.exe binary is in the execution PATH ; and that the postgres
super-user is named "postgres" as usual. In Windows you will need to know the
postgres super-user password, setup on installation.)

As root, type

```bash
sudo su - postgres
cd PROJECT-ROOT/backoffice-sql/lib/sql/install
psql postgres postgres
\i setup.sql
\q
```

(If user postgres does not have privileges to read your project files, just past setup.sql into psql one line at a time).

This will create a Postgres user egovhealth, password egovhealth,
owner of a database named egovhealth. But the database will be empty
(without any tables and without any data), and needs to be populated.

### Instructions to populate the PostgreSQL egovhealth database

Using your regular shell user, feed the SQL file
PROJECT-ROOT/backoffice-sql/lib/sql/install/egovhealth.sql
into the psql command-line client:

```bash
cd PROJECT-ROOT/backoffice-sql/lib/sql/install
psql --host=localhost egovhealth egovhealth
drop owned by egovhealth; -- not needed on initial creation
\i egovhealth.sql
\q
```

PS: The --host=localhost is to prevent the peer authentication issue
https://stackoverflow.com/questions/18664074/getting-error-peer-authentication-failed-for-user-postgres-when-trying-to-ge