# sql/changes

The scripts under this install folder are incremental changes to be applied to the database.

This folder is intended to be used only after production rollout.
Until then, the ../install/egovhealth.sql can be changed, and the whole DB re-created.
