-- #2 - Import synthetic data at
-- https://repository.glass-h2020.eu/remote.php/webdav/GLASS%20project/03%20Work%20Packages/WP6%20%7C%20GLASS%20Enhanced%20Service%20Suites/T6.4%20AI%20Data%20Schema%20Transformer/Synthetic%20Citizens/Exported%20Citizens/final%20export/final%20exports-20220705T142047Z-001.zip
-- as of 2022-07-05 ĩnto table egovhealth.
--
-- Expects this data to be present at ../Synthetic Citizens
-- Edit paths below if not.
-- 
-- Expected way to run this tool (with a blank egovhealth table):
--
-- cd egovhealth
-- psql egovhealth egovhealth -f backoffice-sql/lib/sql/tools/tsvToSqlHealth.sql
--
-- Requires external program iconv to convert from UTF-16LE to UTF-8
--
-- To make imported data permanent, execute
-- egov-health$ pg_dump -h localhost -U egovhealth > backoffice-sql/lib/sql/install/egovhealth.sql 
-- and commit the results.



CREATE OR REPLACE FUNCTION f_str2date(dateStr TEXT) RETURNS DATE AS $$
DECLARE
    v DATE := NULL;
BEGIN
    v := TO_DATE(dateStr, 'YYYYMMDD');
    RETURN v;
EXCEPTION WHEN OTHERS THEN
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION f_str2date IS 'Converts a string in the format YYYYMMDD into a date. Returns NULL if the string is an invalid date.';

DROP TABLE IF EXISTS tmp_citizen;

-- TEXT fields for everything. Import all data, and check/convert format aftwerwards.
CREATE TEMPORARY TABLE tmp_citizen (
  CCV_00002_Name TEXT, -- A spreadsheet column
  CCV_00002_ENG_Name TEXT, -- B
  CCV_00003_Surname TEXT, -- C
  CCV_00003_ENG_Surname TEXT, -- D
  CCV_00004_DateofBirth TEXT, -- E
  CCV_00005_Fathernameandsurname TEXT, -- F
  CCV_00005_ENG_Fathernameandsurname TEXT, -- G
  CCV_00008_Mothernameandsurname TEXT, -- H
  CCV_00008_ENG_Mothernameandsurname TEXT, -- I
  CCV_00011_Citizenship TEXT, -- J
  CCV_00011_ENG_Citizenship TEXT, -- K
  CCV_00012_Nationality TEXT, -- L
  CCV_00013_Sex TEXT, -- M 
  CCV_00014_Height TEXT, -- N
  CCV_00015_Currentaddress TEXT, -- O
  CCV_00015_ENG_Currentaddress TEXT, -- P
  CCV_00083_Insurancestatus TEXT, -- Q
  CCV_00022_IDnumber TEXT, -- R
  CCV_00023_Identitycardissuelocation TEXT, -- S
  CCV_00023_ENG_Identitycardissuelocation TEXT, -- T
  CCV_00024_Identitycardissueregion TEXT, -- U
  CCV_00024_ENG_Identitycardissueregion TEXT, -- W
  CCV_00025_Identitycardissuedate TEXT, -- V
  CCV_00026_Identitycardissuingauthority TEXT, -- X
  CCV_00026_ENG_Identitycardissuingauthority TEXT, -- Y
  CCV_00028_Birthregistryauthority TEXT, -- Z
  CCV_00028_ENG_Birthregistryauthority TEXT, -- AA
  CCV_00029_Birthcertificateissuingauthority TEXT, -- AB
  CCV_00029_ENG_Birthcertificateissuingauthority TEXT, -- AC
  CCV_00030_Birthlocation TEXT, -- AD
  CCV_00030_ENG_Birthlocation TEXT, -- AE
  CCV_00031_Regionofbirth TEXT, -- AF
  CCV_00031_ENG_Regionofbirth TEXT, -- AG
  CCV_00032_Birthcertificateissuedate TEXT, -- AH
  CCV_00034_Passportnumber TEXT, -- AI
  CCV_00035_Passportissuedate TEXT, -- AJ
  CCV_00036_Passportexpirationdate TEXT, -- AK
  CCV_00037_Passportissuelocation TEXT, -- AL
  CCV_00037_ENG_Passportissuelocation TEXT, -- AM
  CCV_00038_Passportissuingcountry TEXT, -- AN
  CCV_00039_Passportissuingauthority TEXT, -- AO
  CCV_00039_ENG_Passportissuingauthority TEXT, -- AP
  CCV_00041_Socialsecuritynumber TEXT, -- AQ
  CCV_00042_Healthinsuranceissuedate TEXT, -- AR
  CCV_00043_Healthinsuranceexpirationdate TEXT, -- AS
  CCV_00045_IncomedeclarationdocumentID TEXT, -- AT
  CCV_00046_Incomedeclarationdate TEXT, -- AU
  CCV_00047_Incomedeclarationfiscalyear TEXT, -- AW
  CCV_00048_Declaredincome TEXT, -- AV
  CCV_00049_Incometax TEXT, -- AX
  CCV_00052_Primaryschoolname TEXT, -- AY
  CCV_00052_ENG_Primaryschoolname TEXT, -- AZ
  CCV_00053_Primaryschooldegreeissuedate TEXT, -- BA
  CCV_00054_Primaryschoolgrade TEXT, -- BB
  CCV_00057_Lowersecondaryschoolname TEXT, -- BC
  CCV_00057_ENG_Lowersecondaryschoolname TEXT, -- BD
  CCV_00058_Lowersecondaryschooldegreeissuedate TEXT, -- BE
  CCV_00059_Lowersecondaryschoolgrade TEXT, -- BF
  CCV_00061_Highersecondaryschoolname TEXT, -- BG
  CCV_00061_ENG_Highersecondaryschoolname TEXT, -- BH
  CCV_00062_Highersecondaryschooldegreeissuedate TEXT, -- BI
  CCV_00063_Highersecondaryschoolgrade TEXT, -- BJ
  CCV_00065_Tertiaryschoolname TEXT, -- BK
  CCV_00065_ENG_Tertiaryschoolname TEXT, -- BL
  CCV_00066_Tertiarydegreeissuedate TEXT, -- BM
  CCV_00067_Tertiaryschoolgrade TEXT, -- BN
  CCV_00068_Tertiaryschooltype TEXT, -- BO
  CCV_00069_ECTSredits TEXT, -- BP
  CCV_00079_Criminalrecordissuedate TEXT, -- BQ
  CCV_00080_Criminalrecordissuingauthority TEXT, -- BR
  CCV_00080_ENG_Criminalrecordissuingauthority TEXT, -- BS
  CCV_00081_Existenceofcriminalrecord TEXT, -- BT
  CCV_00082_Criminalrecordexpirationdate TEXT, -- BU
  CCV_00071_Totaldisabilityvalue TEXT, -- BV
  CCV_00072_Disabilityreportissuedate TEXT, -- BW
  CCV_00073_Disabilityreportissuingauthority TEXT, -- BX
  CCV_00073_ENG_Disabilityreportissuingauthority TEXT, -- BY
  CCV_00084_Disabilityreportexpirationdate TEXT, -- BZ
  CCV_00074_Disabilityrecord1 TEXT, -- CA
  CCV_00074_ENG_Disabilityrecord1 TEXT, -- CB
  CCV_00074_Disabilityrecord2 TEXT, -- CC
  CCV_00074_ENG_Disabilityrecord2 TEXT, -- CD
  CCV_00074_Disabilityrecord3 TEXT, -- CE
  CCV_00074_ENG_Disabilityrecord3 TEXT, -- CF
  CCV_00074_Disabilityrecord4 TEXT, -- CG
  CCV_00074_ENG_Disabilityrecord4 TEXT, -- CH
  CCV_00074_Disabilityrecord5 TEXT, -- CI
  CCV_00074_ENG_Disabilityrecord5 TEXT, -- CJ
  CCV_00074_Disabilityrecord6 TEXT, -- CK
  CCV_00074_ENG_Disabilityrecord6 TEXT -- CL
);

CREATE OR REPLACE PROCEDURE p_mvTmp2egovhealth(aCountryCode VARCHAR) AS $$
DECLARE
   aRc INTEGER;
BEGIN
   -- delete headers (kept on import just to check)
   -- seems that there are extra chars 
   DELETE FROM tmp_citizen WHERE ccv_00002_name LIKE 'CCV%00002(Name)%'; 
   
   INSERT INTO egovhealth ( 
      id,
      givenname,
      surname,
      insuranceIssueDate,
      insuranceExpirationDate,
      insuranceStatus,
      birthDate,
      nationality,
      gender,
      country
    )
   SELECT 
      CCV_00022_IDnumber,
      CCV_00002_name,
      COALESCE(CCV_00003_Surname, '-'), -- There is one citizensurname!!! Workaround: Use '-'
                                         -- DETAIL:  Failing row contains (ΗΗ 003971, Ζωή, null, 1974-10-29, Αθήνα, Αττικής, ΑΤ, Αθήνα, Πυθαγόρας , Χριστίνα Ονγκάρη, 1961-12-26, Αθήνα, Αττικής, Αττικής, Αθήνα, Greek, Female, 1.33, Αθήνα, Βενιζέλου Σοφοκλή 76, 10485, null).
      f_str2date(CCV_00042_Healthinsuranceissuedate), 
      f_str2date(CCV_00043_Healthinsuranceexpirationdate), 
      CCV_00083_Insurancestatus,
      f_str2date(CCV_00004_DateofBirth),
      CCV_00012_Nationality,
      CCV_00013_Sex,
      aCountryCode 
   FROM tmp_citizen
   WHERE CCV_00022_IDnumber!='-'
     AND NOT EXISTS (SELECT id FROM egovhealth WHERE id=CCV_00022_IDnumber);

   GET DIAGNOSTICS aRc = ROW_COUNT;
   RAISE NOTICE '% %',aCountryCode,aRc;
   DELETE FROM tmp_citizen;
   RETURN;
END;  
$$ LANGUAGE plpgsql;


-- paths or external converstion program might need tweaking
-- format now seems to be TSV
\COPY tmp_citizen FROM PROGRAM 'iconv -f utf-16le -t utf-8 < _20220705-160450.txt' (FORMAT CSV, DELIMITER E'\t'); -- Greek citizens
DO $$ BEGIN CALL p_mvTmp2egovhealth('GR'); END; $$ LANGUAGE plpgsql;

-- No need to generate a rejection report, for the moment.

\COPY tmp_citizen FROM PROGRAM 'iconv -f utf-16le -t utf-8 < _20220705-160526.txt' (FORMAT CSV, DELIMITER E'\t'); -- Portuguese citizens
DO $$ BEGIN CALL p_mvTmp2egovhealth('PT'); END; $$ LANGUAGE plpgsql;

\COPY tmp_citizen FROM PROGRAM 'iconv -f utf-16le -t utf-8 < _20220705-155847.txt' (FORMAT CSV, DELIMITER E'\t'); -- Turkish citizens
DO $$ BEGIN CALL p_mvTmp2egovhealth('TK'); END; $$ LANGUAGE plpgsql;

DROP PROCEDURE p_mvTmp2egovhealth;