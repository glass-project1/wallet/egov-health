#!/bin/bash -x
# this is used by ../docker/docker-compose.yml
# it runs (inside the docker only) the egovhealth.sql script with the user egovhealth
set -e
# remove ON_ERROR_STOP=1 to ignore errors
#psql -v ON_ERROR_STOP=1 --username "egovhealth" --dbname "egovhealth" -f /docker-entrypoint-initdb.d/60egovhealth.norun
# removed because there is an error on a COMMENT, ERROR:  must be owner of extension uuid-ossp
psql -v --username "egovhealth" --dbname "egovhealth" -f /docker-entrypoint-initdb.d/60egovhealth.norun
if [ -n "$GLASS_COUNTRY" ]
then
    if [[ ! "$GLASS_COUNTRY" =~ [A-ZA-Z] ]] ; then echo 2>&1 "env GLASS_COUNTRY must be an ISO 2 letter code upper-case" ; exit 1 ; fi
    GLASS_COUNTRY_LC=$(echo "$GLASS_COUNTRY" | tr '[:upper:]' '[:lower:]')
    psql --echo-all -v --username "egovhealth" --dbname "egovhealth" <<EOF
DELETE FROM egovhealth WHERE country != '${GLASS_COUNTRY}';
UPDATE appresource
    SET value='http://egov-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egovhealth.parent.borestUrl';
EOF
else
    echo "WARNING: environment variable GLASS_COUNTRY not set! Preserving default appresource URLs" 2>&1
fi
