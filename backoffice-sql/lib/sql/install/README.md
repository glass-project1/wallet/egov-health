# sql/install

The scripts under this install folder are supposed to run only once, when the database is setup for the 1st time.

## Files

```sh
setup.sql - create user and databse egovhealth (to run as PostgreSQL superuser - typically postgres)
egovhealth.sql - create and populate egovhealth database (to run as egovhealth user)
egovhealth_run.sh - to be used from the top level Dockerfile. Do not run manually.
```