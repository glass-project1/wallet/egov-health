#!/bin/bash -xe
# Tool to reset database in egov-health
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov-health ]
then
    echo 1>&2 "Current working dir must be egov-health"
    exit 1
fi
cd backoffice-sql
PGPASSWORD=egovhealth psql -h localhost egovhealth egovhealth <<EOF
DROP OWNED BY egovhealth;
\i lib/sql/install/egovhealth.sql
EOF
